#include "sim.h"

#define STEP_MULT 0.001
#define MIN_STEP_SIZE (0.001*STEP_MULT)
#define STEPS 50000

void step(scalar b, scalar& r, scalar& phi, scalar& dir, bool& swapped){
	scalar sq = ((r * r)/(b * b) - (1 - R/r));
	scalar derivate_r_on_phi = r * sqrt(sq);
	scalar step_size = derivate_r_on_phi * STEP_MULT / r;// (r * r * r);
	phi += step_size / derivate_r_on_phi;
	if(!swapped && step_size < MIN_STEP_SIZE){
		swapped = true;
		dir = 1;
	}
	r += step_size * dir;
}

void render_light_path(scalar r, scalar phi, scalar angle_to_center, vector<vector<Color>>& img){
	int img_sx = img.size();
	int img_sy = img[0].size();

	scalar dir = -1;

	if(fabs(angle_to_center) > M_PI)dir = 1;
	scalar b = r / sqrt(1-R/r) * sin(angle_to_center) - r * 0.0000000000000000001;

	bool swapped = false;
	for(size_t i = 0; i < STEPS; i++){
		if(r < R)return;
		step(b, r, phi, dir, swapped);

		int x = img_sx/2 + sin(phi) * r * RD;
		int y = img_sy/2 + cos(phi) * r * RD;

		if(x < 0 ||x >= img_sx)continue;
		if(y < 0 ||y >= img_sy)continue;

		img[x][y] = 1;
	}
	return;
}

///Returns 0 on black hole
///returns 1 for infinity
///returns values inbetween for place in disc
scalar ray_dist(scalar r, scalar phi, scalar sin_angle_to_center, scalar dir, scalar disk_min, scalar disk_max){
	scalar b = r / sqrt(1-R/r) * sin_angle_to_center - r * 0.0000000000000000001;

	bool swapped = false;
	for(;;){
		step(b, r, phi, dir, swapped);
		if(r < R * 1.5 && dir < 0)return 0;
		if(r > disk_max && dir > 0) return 1;
		if(phi > 0){
			if(r > disk_min && r < disk_max){
				return (r-disk_min) / (disk_max - disk_min);
			}
			
			phi -= M_PI;
		}
	}
	return 0;
}
