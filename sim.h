#pragma once
#include "common.h"

void render_light_path(scalar phi, scalar r, scalar angle_to_center, vector<vector<Color>>& img);
scalar ray_dist(scalar r, scalar phi, scalar sin_angle_to_center, scalar dir, scalar disk_min, scalar dick_max);
