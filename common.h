#pragma once
#include <bits/stdc++.h>

using namespace std;

using scalar = long double;

#define R 1.
#define RD 64

struct Color{
	Color(){}
	Color(float grey): r(grey), g(grey), b(grey){}
	Color(float r, float g, float b): r(r), g(g), b(b){}
	float r, g, b;
};
