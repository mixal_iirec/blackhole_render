#include "draw.h"
#include <Magick++.h>

void draw(const char* filename, vector<vector<Color>>& img){
	Magick::Image im(Magick::Geometry(img.size(), img[0].size()), "black");
	/*im.fillColor("green");
	size_t sx = img.size();
	size_t sy = img.size();
	int radius = RD/sqrt(2);
	im.draw(Magick::DrawableCircle(sx/2, sy/2, sx/2+radius, sy/2+radius));*/

	for(size_t x = 0; x < img.size(); x++){
		for(size_t y = 0; y < img[0].size(); y++){
			//if(img[x][y].r || img[x][y].g || img[x][y].b)
			im.pixelColor(x, y, Magick::ColorRGB(img[x][y].r, img[x][y].g, img[x][y].b));
		}
	}

	im.write(filename);
}
