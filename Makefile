SOURCES = $(patsubst ./%,%,$(shell find ./ -name "*.cpp" | egrep -v './.+main.cpp'))
SOURCES_C = $(patsubst ./%,%,$(shell find ./ -name "*.c" | egrep -v './.+main.c'))

OBJS = $(patsubst %.cpp,o/%.o,$(SOURCES))
DEPS = $(patsubst %.cpp,d/%.d,$(SOURCES))

OBJS_C = $(patsubst %.c,o/%.o,$(SOURCES_C))
DEPS_C = $(patsubst %.c,d/%.d,$(SOURCES_C))


INCLUDES = $(shell cat '.makefile_set/libraries') $(shell cat '.makefile_set/modules' | sed "s/^/modules\//g")

CCFLAGS = $(shell cat .makefile_set/flags_c .makefile_set/flags_both) $(addprefix -I, $(INCLUDES))
CXXFLAGS = $(shell cat .makefile_set/flags_c++ .makefile_set/flags_both) $(addprefix -I, $(INCLUDES))
CXXFLAGSOUT = $(shell cat .makefile_set/libraries_comp)

all: run

release: CXXFLAGS+=-DNDEBUG
release: CCFLAGS+=-DNDEBUG
release: clean run

run: $(OBJS) $(OBJS_C)
	$(CXX) -o run $^ $(CXXFLAGSOUT)

d/%.d: %.cpp
	$(CXX) $(CXXFLAGS) -MM -MT '$(patsubst %.cpp,o/%.o,$<)' $< -MF $@
	echo '	$(CXX) $$(CXXFLAGS)'" -o $(patsubst %.cpp,o/%.o,$<) -c $<" >> $@

d/%.d: %.c
	$(CC) $(CCFLAGS) -MM -MT '$(patsubst %.c,o/%.o,$<)' $< -MF $@
	echo '	$(CC) $$(CCFLAGS)'" -o $(patsubst %.c,o/%.o,$<) -c $<" >> $@

include $(DEPS) $(DEPS_C)

clean:
	find ./ -name "*.d" -delete
	find ./ -name "*.o" -delete
	rm run

#to update dependences if changed in header
update:
	find ./ -name "*.d" -delete

doc:
	doxygen doxygen.cfg
