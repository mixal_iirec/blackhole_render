#include "common.h"
#include "draw.h"
#include "sim.h"
#include "render.h"

int main(int argc, const char* argv[]){
	/*vector<vector<Color>> a(1024, vector<Color>(1024, 0));
	a[512][512] = 0.5;
	a[512-1][512] = 0.5;
	a[512+1][512] = 0.5;
	a[512][512-1] = 0.5;
	a[512][512+1] = 0.5;

	ray_dist(R * 2, M_PI/180 * 90, M_PI/2, a);
	ray_dist(R * 2, M_PI/180 * 90, M_PI/2-M_PI/180 * 40, a);

	draw("file.png", a);*/

	//auto o = look(512, 512, 10, M_PI/180 * 10);
	auto o = look(1024, 1024, 12, M_PI/180 * 20);
	//auto o = look(128, 128, 12, M_PI/2 + M_PI/180 * 30);
	draw("look.png", o);

	return 0;
}
