#include "render.h"

#define DISK_MIN 3.
#define DISK_MAX 8.

vector<vector<Color>> look(size_t szx, size_t szy, scalar r, scalar psi){
	scalar sin_psi = sin(psi);
	scalar cos_psi = cos(psi);
	vector<vector<Color>> a(szx, vector<Color>(szy, 0));
	for(size_t ix = 0;ix < szx; ix++){
		cout << "X: " << ix << endl;
		for(size_t iy = 0;iy < szy; iy++){
			scalar x = -1 + ix * 2./(szx - 1.);
			scalar y = -1 + iy * 2./(szy - 1.);
			scalar norm = sqrt(x * x + y * y + 1);
			x/=norm;
			y/=norm;

			scalar h_move = sqrt(x * x + y * y);

			scalar angle_to_disk = atan2(sin_psi * h_move, cos_psi * y);
			scalar ret = ray_dist(r, -angle_to_disk, h_move, -1., DISK_MIN, DISK_MAX);
			if(ret == 0)a[ix][iy] = 0;
			else if(ret == 1)a[ix][iy] = 0;
			else a[ix][iy] = Color(0, 1-ret, ret);
		}
	}
	return a;
}
