#pragma once
#include "draw.h"
#include "sim.h"

vector<vector<Color>> look(size_t szx, size_t szy, scalar r, scalar psi);
